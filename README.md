# README #

**Welcome to my solution to Trust Pilot's Backend Code Challenge: Follow the White Rabbit - The Red Pill.** 

I made this web application without reading the teaser/tip as I wanted to see what I could come up with on my own. I ran out of debugging time unfortunately so it is an incomplete solution, but there should be enough work there to give you an idea of my search algorithm. I merged all uncompleted branches into master for ease of reading/running locally (otherwise I would have left it inside the branch until it was fully tested and debugged).

### Work Log ###

* **Total Hours Coded:** 25.5 hrs
* **Total Days Coded:** 6 (online)
* **Total Days Thought on Design:** 2 (offline)

### Design Notes ###

I break the challenge down into three main parts: 

1. Importing word list and filtering out impossible words,
2. Combining words into possible phrases and checking it's hash,
3. The unit tests and making the code production ready and easier to maintain.

### Features ###

* I've tried to keep the number of classes/objects to a minimum to avoid overhead in memory allocation/processing time.
* I've tried to reuse variables as much as possible and initialise them only once to reduce overhead in memory allocation/processing time.
* I've used a recursive function to combine words into combinations that are then tested to see if they can be a phrase made out of the anagram.
* I've made my design based on the idea that we want to reduce as much as possible the number of times we have to loop/call recursively in the search algorithm so that the program stops the moment we find a match to save on overhead. This is critical!
* I never process/test a combination more than once - even during recursive calls.
* I check if it's possible to add any more words or letters to the current combinations I'm trying and if not then skip to next possible combination in search algorithm.
* The algorithm works with duplicate words in the same phrase if there are enough letters to allow for it in anagram.
* Makes no assumptions on the number of words in the secret phrase.

### Outstanding ###

* Debug recursive loop all the way to a match. Right now when I run the code it will give me a *StackOverflowException: The call stack contains only external code* error eventually. For some reason it doesn't cause the Home/Index page to load (like it had when debugging in the past). I've looked up the error online and I came up with this so far for inspiration: http://stackoverflow.com/questions/17833604/the-call-stack-contains-only-external-code

### Code Notes ###

* I commented out all the Debug.Write messages in order to avoid some exceptions when writing to the Output in a recursive loop that has to iterate quite a lot. However if you uncomment them you can follow the progression through the recursive loop/search algorithm for a while.

### Glossary ###

* **Secret Phrase:** The final result we are looking for that matches the hash given and is made up of all the letters in the anagram.
* **Possible Combination:** A combination of words that, when you put together all their letters, they can be found entirely within the anagram. So potential Secret Phrase.
* **Tried Combination:** A combination of words we have already tested to see if it is a possible combination, but it failed the test. These we can skip in future tests/loops.
* **Possible Word:** A word, all the letters of which can be found within the anagram, without any remaining letters left (i.e. no impossible letters). 
* **Next Combination:** The next combination of words we are trying to test to determine of it can be a Possible Combination.  This is what we return when we have a match in recursive NextCombination function.
* **Combination Index:** The pointer to the current and latest Possible Combination in list.
* **Word Index:** The pointer to the current and latest Possible Word in list.
* **Word Count:** The pointer to the current and latest Possible Word to be tested along with Next Combination. 

### Who do I talk to? ###

* Niamh Brown - dev@hermitstudio.com