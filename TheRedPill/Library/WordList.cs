﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace TheRedPill.Library
{
    public class WordList
    {
        private string anagram;
        private char[] anagramUnique;
        private string wordFilePath;
        private List<string> fullContents = new List<string>();
        private List<string> filteredContents = new List<string>();

        /// <summary>WordList(string paramAnagram, string paramFilePath)
        /// <para>Initialise TheRedPhill\Library\WordList Object.</para>
        /// </summary>
        public WordList(string paramAnagram, string paramFilePath)
        {
            anagram = paramAnagram;
            wordFilePath = paramFilePath;

            if (!String.IsNullOrEmpty(paramAnagram))
            {
                anagramUnique = anagram.ToCharArray().Distinct().ToArray();
            }            
        }

        /// <summary>List<string> importWordList(bool filter = true)
        /// <para>Import the word list and filter out impossible words.</para>
        /// </summary>
        public List<string> importWordList(bool filter = true)
        {
            
            String filePath = AppDomain.CurrentDomain.BaseDirectory + wordFilePath.ToString();

            Debug.Write("Importing Words From File: " + filePath.Replace(@"\", @"/") + "\n");
            
            // Read in contents of word list file (Note: StreamReader.ReadLine in loop is faster than File.ReadAllLines)
            // Code amended from: http://stackoverflow.com/questions/7387085/how-to-read-an-entire-file-to-a-string-using-c-sharp
            using (StreamReader sr = File.OpenText(filePath.Replace(@"\", @"/")))
            {
                string s = String.Empty;
                int fullCount = 0;

                while ((s = sr.ReadLine()) != null)
                {                    
                    if (!String.IsNullOrEmpty(s))
                    {
                        fullContents.Add(s);
                        fullCount++;                        
                    }
                }
            }

            Debug.Write("Number of Words Imported: " + fullContents.Count + "\n");

            // If requested, filter out impossible words from the word list
            if (filter)
            {
                int filtercount = 0;

                // Loop through words list in parallel to speed up processing time
                Parallel.For(0, fullContents.Count, x =>
                {
                    if (isValidWord(fullContents[x]))
                    {
                        filteredContents.Add(fullContents[x]);
                        filtercount++;
                    }                    
                });

                Debug.Write("Number of Words Filtered: " + filteredContents.Count + "\n");

                return filteredContents;
            }
            else
            {
                return fullContents;
            }          
        }

        /// <summary>bool isValidWord(string word)
        /// <para>Check to see if all the letters of the word can be found within the anagram and is therefore valid.</para>
        /// </summary>
        public bool isValidWord(string word)
        {
            // Remove all the distinct anagram characters (that are valid) from the word to see what is left            
            foreach (char letter in anagramUnique)
            {
                word = word.Replace(letter.ToString(),String.Empty);
            }

            // Only words with no letters remaining can be a possible combination
            if (word.ToString() == String.Empty)
            {
                return true;
            }
            else
            {
                // This word is an impossible part of the anagram as it contains characters that are NOT in the anagram
                return false;
            }            
        }

        /// <summary>static List<string> SubSet(List<string> list, string remainingAnagram)
        /// <para>Get a Subset of a List</para>
        /// </summary>
        public static List<string> SubSet(List<string> list, string remainingAnagram) {

            List<string> filteredList = new List<string>();
            char[] anagramUnique;

            //Debug.Write("SubSet: checking remaining anagram '" + remainingAnagram + "' \n");

            if (!String.IsNullOrEmpty(remainingAnagram))
            {
                anagramUnique = remainingAnagram.ToCharArray().Distinct().ToArray();

                foreach (string word in list)
                {
                    string compareWord = word;

                    foreach (char letter in anagramUnique)
                    {
                        compareWord = compareWord.Replace(letter.ToString(), String.Empty);
                    }

                    // Only words with no letters remaining are part of the sub-set as they fit withing the remaining anagram letters
                    if (compareWord.ToString() == String.Empty)
                    {
                        filteredList.Add(compareWord);
                    }
                }
            }

            return filteredList;
        }

        /// <summary>int ImportedWordCount()
        /// <para>Get the total number of imported words.</para>
        /// </summary>
        public int ImportedWordCount()
        {
            return fullContents.Count;
        }
    }
}