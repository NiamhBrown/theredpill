﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace TheRedPill.Library
{
    public class WordCombiner
    {
        List<string> possibleWords = new List<string>(); // DON'T CHANGE THIS AFTER IT IS SET IN CONSTRUCTOR!
        private List<string[]> possibleCombinations = new List<string[]>();
        private List<string[]> triedCombinations = new List<string[]>();
        List<string> nextCombination = new List<string>();
        List<int> wordIndex = new List<int>();   
        int combinationIndex = 0;
        int wordCount = 0;
        string anagram;

        /// <summary>WordCombiner(List<string> paramPossibleWords, string paramAnagram)
        /// <para>Initialise TheRedPhill\Library\WordCombiner Object.</para>
        /// </summary>
        public WordCombiner(List<string> paramPossibleWords, string paramAnagram)
        {
            possibleWords = paramPossibleWords;
            anagram = paramAnagram;
        }

        /// <summary>string NextCombination()
        /// <para>RECURSIVE METHOD: Find The Next Combination of Words That Could Be a Possible Phrase to be Further Processed by the System</para>
        /// </summary>
        public string NextCombination()
        {           
            string nextWord = "";

            //Debug.Write("Next Combination: STARTING POINT \n");

            // If no words in next combination 
            if (nextCombination.Count == 0)
            {
                // Then add next possible word + store data to find that word again in all possible words if it turns out to be correct
                AddToNextCombination(possibleWords.ElementAt(wordCount));

                //Debug.Write("Next Combination: NEW COMBINATION (1) '" + BuildPhrase(nextCombination) + "' \n");

                // Recall recurve method to find next word in phrase (todo: do we need the return??)
                return NextCombination();
            }
            else
            {
                // See if there is another possible word left to add to this combination
                if (possibleWords.Count >= wordCount)
                {
                    nextWord = possibleWords.ElementAt(wordCount);

                    // Add the next possible word to current combination to check
                    AddToNextCombination(nextWord);

                    //Debug.Write("Next Combination: NEW COMBINATION (2) '" + BuildPhrase(nextCombination).ToString() + "' \n");

                    // Otherwise check if we have tried this combination before
                    if (!IsKnownCombination(nextCombination))
                    {
                        //Debug.Write("Next Combination: Combination is not known - test it! \n");

                        // If not, then see if every letter in this word (including duplicate letters) can be found in the anagram
                        if(IsWholeWordInAnagram(nextWord)) {

                            //Debug.Write("Next Combination: Is '"+ nextWord +"' in '" + anagram + "'? \n");

                            // Check to see if entire anagram has been found in this combination
                            if (IsPossiblePhrase(nextCombination)) {

                                //Debug.Write("Next Combination: WE FOUND A POSSIBLE PHRASE! \n");

                                // If so, then add word to possible combinations list and save it
                                possibleCombinations.Add(nextCombination.ToArray());

                                // Move the combination index to the next empty slot for the next time the function is called
                                combinationIndex++;

                                //Debug.Write("Next Combination: END POINT - MATCH FOUND \n");

                                // EXIT - Stop recursive loop and return combination for processing :)
                                return nextCombination.ToString();                                
                            }

                        } else {

                            // Otherwise, see if we can find another word to add to this combination (recursive)
                            //Debug.Write("Next Combination: END POINT - ADD NEXT WORD TO COMBINATION \n");

                            // recursive
                            return NextCombination();
                        }                        
                    }
                    else
                    {                        
                        if (nextCombination.Count > 0) {

                            // Do we have any more letters left to add words with (i.e. so we can try adding another word to current combination being tested)
                            if (BuildPhrase(nextCombination).Replace(" ", "").Length >= anagram.Replace(" ", "").Length)
                            { 
                                // If not, then restart the combination so we don't try it again as we don't have this many letters in the secret phrase
                                ResetNextCombination();

                                //Debug.Write("Next Combination: END POINT - COMBINATION RESTART (1) \n"); 

                                // recursive
                                return NextCombination();
                            }
                            else { 

                                // We can add more words - so skip this combination state and try and find another word to add to it (recursive)
                                triedCombinations.Add(nextCombination.ToArray());

                                //Debug.Write("Next Combination: END POINT - TRIED COMBINATION \n");                                

                                // recursive
                                return NextCombination();
                            }
                        }
                        else
                        {
                            // Then just go ahead and setup the new starting point - no need to reset
                            //Debug.Write("Next Combination: END POINT - COMBINATION EMPTY \n");

                            // recursive
                            return NextCombination();
                        }                         
                    }     

                } else {

                    // Otherwise, we couldn't find another word to add to this combination - so it can't be a possibility
                    //Debug.Write("Next Combination: END POINT - COMBINATION IMPOSSIBLE \n");

                    // Check to see if we have run out of possible words to start from
                    if (possibleWords.Count == wordIndex.Count)
                    {
                        // EXIT - We have nothing left to try! :(
                        //Debug.Write("Next Combination: END POINT - COMBINATION UNCOMPLETABLE \n");

                        return String.Empty;
                    }
                    else { 

                        // Otherwise, reset combination and recall recurve method to find next starting word in phrase
                        ResetNextCombination();

                        //Debug.Write("Next Combination: END POINT - COMBINATION RESTART (2) \n");

                        // recursive
                        return NextCombination();
                    }
                }
            }

            //Debug.Write("Next Combination: END POINT - EOL \n");

            return String.Empty; // ???
        }

        /// <summary>string BuildPhrase(List<string> combination)
        /// <para>Build a string phrase from a list of words.</para>
        /// </summary>
        public string BuildPhrase(List<string> combination)
        {
            string phrase = "";

            //Debug.Write("BuildPhrase: phrase = '' \n");

            foreach (string word in combination)
            {
                phrase = phrase + " " + word;
                //Debug.Write("BuildPhrase: phrase = '" + phrase.TrimStart().ToString() + "' \n");
            }

            return phrase.TrimStart().ToString();
        }

        /// <summary>void AddToNextCombination(string word)
        /// <para>Add a word to the next combination to test.</para>
        /// </summary>
        public void AddToNextCombination(string word)
        {
            //Debug.Write("AddToNextCombination: Adding word '" + word + "' to combination '" + BuildPhrase(nextCombination) + "' \n");

            nextCombination.Add(word);
            triedCombinations.Add(nextCombination.ToArray());
            wordIndex.Add(wordCount);
            wordCount++;
        }

        /// <summary>void ResetNextCombination()
        /// <para>Reset new combination so we can search from new starting point.</para>
        /// </summary>
        public void ResetNextCombination()
        {
            triedCombinations.Add(nextCombination.ToArray());
            int lastIndex = wordIndex.Count();

            if (lastIndex >= 1)
            {
                wordIndex.RemoveAt(lastIndex - 1);
            }

            nextCombination.Clear();
            wordCount = 0;

            //Debug.Write("ResetNextCombination: RESET COMPLETE! \n");
        }

        /// <summary>bool IsPossiblePhrase(List<string> combination)
        /// <para>Check a combination of words to see if they fit entirely within an anagram and are therefore a possible combination/phrase.</para>
        /// </summary>
        public bool IsPossiblePhrase(List<string> combination)
        {
            string remainingLetters = anagram;

            // Remove each word in combination from the agram
            foreach (string word in combination)
            {
                if (IsWholeWordInAnagram(word))
                {
                    remainingLetters = RemoveWord(word, remainingLetters);
                }
                else
                {
                    // If we can't find the word in the phrase then this can't be a possibility
                    return false;
                }
            }

            // if all words in combination can be found in the anagram of the secret phrase then we have a possible phrase to process
            if (remainingLetters.Trim() == String.Empty)
            {
                // We found a possible phrase to process!
                return true;
            }
            else
            {
                // otherwise this can't be a possible phrase :(
                return false;
            }
        }

        /// <summary>bool IsWholeWordInAnagram(string word) 
        /// <para>Check if every letter in a word is within the anagram (including duplicate letters).</para>
        /// </summary>
        public bool IsWholeWordInAnagram(string word) // Including duplicate letters
        {
            string remainingLetters = RemoveWord(word, anagram);
            bool result = true;

            // if all letters in word were removed then the combined sizes should match after removal
            if (remainingLetters.Length + word.Length != anagram.Length)
            {
                result = false;
            }

            return result;
        }

        /// <summary>string RemoveWord(string word, string phrase) 
        /// <para>Remove all the letters in a word (inclduing duplicates) from the anagram and return what is leftover.</para>
        /// </summary>
        public string RemoveWord(string word, string phrase)
        {
            //Debug.Write("RemoveWord with combination '" + word + "' and anagram '" + anagram + "' - char array = " + word.ToCharArray().ToString() + " \n");

            string remainingLetters = phrase;

            // Remove all the distinct anagram characters (that are valid) from the word to see what is left            
            foreach (char letter in word.ToCharArray())
            {
                //Debug.Write("RemoveWord: Test 1 - with letter '" + letter + "' ");

                // Ignore spaces because we don't known how many words we are dealing with
                if (String.IsNullOrWhiteSpace(letter.ToString()) == false && remainingLetters.IndexOf(letter) != -1)
                {
                    remainingLetters = remainingLetters.Remove(remainingLetters.IndexOf(letter), 1);
                    //Debug.Write("RemoveWord: Test 2 - remianing letters = '" + remainingLetters + "' ");
                }
            }
            //Debug.Write("RemoveWord: Test 3 (done) \n");
            return remainingLetters;
        }

        /// <summary>bool IsKnownCombination(List<string> combination)
        /// <para>Check to see if we have tested this combination of words before. If so, then we can skip testing it again.</para>
        /// </summary>
        public bool IsKnownCombination(List<string> combination)
        {
            bool result = false;

            //Debug.Write("IsKnownCombination: START - Checking Combination '" + BuildPhrase(combination) + "' \n");

            foreach (var phrase in triedCombinations)
            {
                int wordsMatched = 0;

                //Debug.Write("IsKnownCombination: LOOP 1 - Checking Phrase \n");

                // Loop through each word in this possible combination and see if we get a full match
                for (int i = 0; i < phrase.Length; i++) // DEBUG was ++i
                {
                    //Debug.Write("IsKnownCombination: LOOP 2 - Iteration #" + i.ToString() + " \n");

                    foreach(string word in combination) {

                        //Debug.Write("IsKnownCombination: LOOP 3 - Checking Word If '" + word + "' Is In Phrase '" + BuildPhrase(combination) + "' \n");

                        // this word is found in the current possible combination so we may have a match
                        if(phrase[i] == word) {

                            //Debug.Write("IsKnownCombination: LOOP 3 - Word In Current Phrase! \n");
                            wordsMatched++;
                        }

                        // if we have the same number of matched words as there are i the phrase then we have a match
                        if(wordsMatched == phrase.Length) {

                            //Debug.Write("IsKnownCombination: END POINT - MATCH! (Known Phase) \n");
                            result = true;
                            return result;
                        }
                    }                    
                }
            }

            // If we get this far there is no match yet 
            //Debug.Write("IsKnownCombination: END POINT - NO MATCH! (Unknown Phase) \n");
            return result;
        }

        /// <summary>List<string> getPossibleCombinations()
        /// <para>Return a list of all possible combinations found.</para>
        /// </summary>
        public List<string> getPossibleCombinations()
        {
            List<string> buildCombinations = new List<string>();

            foreach (string[] combination in possibleCombinations)
            {
                string str = "";

                foreach (string word in combination)
                {
                    str = str + " " + word;
                }

                buildCombinations.Add(str.Trim().ToString());
            }

            return buildCombinations;
        }       
    }
}