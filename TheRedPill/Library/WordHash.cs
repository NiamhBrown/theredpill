﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace TheRedPill.Library
{
    public class WordHash
    {
        private String hashString = "";
        private MD5 hash = MD5.Create();
        private StringBuilder stringBuilder = new StringBuilder();
        private StringComparer comparer = StringComparer.OrdinalIgnoreCase;

        /// <summary>void WordHash(String paramHash) 
        /// <para>Initialise TheRedPhill\Library\WordHash Object.</para>
        /// </summary>
        public WordHash(String paramHash)
        {
            this.hashString = paramHash;
        }

        /// <summary>string GetHash(string input) 
        /// <para>Get the MD5 hash of a string input.</para>
        /// <seealso cref="https://msdn.microsoft.com/en-us/library/system.security.cryptography.md5(v=vs.110).aspx"/>
        /// </summary>
        string GetHash(string input)
        {
            this.stringBuilder.Clear();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = this.hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                this.stringBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return this.stringBuilder.ToString();
        }

        /// <summary>string GetHash(string input) 
        /// <para>Check if the MD5 hash of a string input matches the anagram.</para>
        /// <seealso cref="https://msdn.microsoft.com/en-us/library/system.security.cryptography.md5(v=vs.110).aspx"/>
        /// </summary>
        public bool CheckHash(string word)
        {
            if (0 == this.comparer.Compare(this.GetHash(word), hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }        
    }
}