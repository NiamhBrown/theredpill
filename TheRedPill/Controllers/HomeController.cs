﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Diagnostics;
using TheRedPill.Library;

namespace TheRedPill.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Debug.Write("Home Controller: START STOPWATCH \n");

            WordList list = new WordList(ConfigurationManager.AppSettings["SecretPhraseAnagram"], ConfigurationManager.AppSettings["WordListFilePath"]);
            WordHash hash = new WordHash(ConfigurationManager.AppSettings["SecretPhraseMD5Hash"]);            

            // Make sure we catch exceptions as this is an intensive program to run
            try
            {
                ///////////////////////////////////////////////////////////
                // IMPORT AND FILTER WORD LIST TO ONLY POSSIBLE WORDS

                Debug.Write("Home Controller: Before Import Word List \n");

                // Import word list and filter out impossible words
                List<string> allPossibleWords = list.importWordList(true);
                ViewBag.PossibleWords = allPossibleWords;
                ViewBag.PossibleWordCount = allPossibleWords.Count;
                ViewBag.PossibleCombinationCount = 0;
                ViewBag.ImportedWordCount = list.ImportedWordCount();

                Debug.Write("Home Controller: After Import Word List \n");

                ///////////////////////////////////////////////////////////
                // FIND POSSIBLE WORD COMBINATIONS (PHRASES)

                WordCombiner combine = new WordCombiner(allPossibleWords, ConfigurationManager.AppSettings["SecretPhraseAnagram"]);
                string nextPhrase = "";
                string secretPhrase = "Unkown Result";
                bool isMatchFound = false;
                int maxIterations = int.Parse(ConfigurationManager.AppSettings["maxIterations"]); 
                int countIterations = 0;

                Debug.Write("Home Controller: STARTING POINT - Search \n");

                // Loop through possible word combinations (phrases) and check against hash + Avoid Infinate Loop!
                while (isMatchFound == false && countIterations <= maxIterations)
                {
                    Debug.Write("Home Controller: Before Find Next Combination \n");

                    nextPhrase = combine.NextCombination();

                    Debug.Write("Home Controller: After Find Next Combination \n");

                    // Check if we have run out of combinations and no more left to try. 
                    if (String.IsNullOrEmpty(nextPhrase))
                    {
                        // Stop Searching - We failed :(
                        Debug.Write("Home Controller: END POINT - NO COMBINATIONS LEFT :( \n");
                        break;
                    }

                    // Otherwise check hash of the next combination of words (phrase) to be found
                    if (hash.CheckHash(nextPhrase) == true)
                    {
                        // We found the match - High Five!
                        secretPhrase = nextPhrase;
                        isMatchFound = true;

                        Debug.Write("Home Controller: END POINT - MATCH FOUND :) \n");

                        break;
                    }
                    else
                    {
                        // DEBUG ONLY   
                        Debug.Write("Home Controller: END POINT - NO HASH MATCH :( \n");
                    }

                    countIterations++;
                }

                Debug.Write("Home Controller: END POINT - Search \n");

                ///////////////////////////////////////////////////////////
                // VIEW DATA SETUP

                // Check to see if we ran out of possibilites (and filtered word list wrong) or if we just hit our infinate loop avoidance limit (so we can tweak parameters + retry)
                if (countIterations > maxIterations)
                {                    
                    secretPhrase = "Unknown Result (Hit Max Iteration Limit)";

                    Debug.Write("Home Controller: END POINT - MAX ITERATIONS \n");

                }

                // Save Details of Possible Combinations We Have Found So We Can Display It To User
                List<string> possibleCombinations = new List<string>();
                possibleCombinations = combine.getPossibleCombinations();
                ViewBag.PossibleCombinationCount = possibleCombinations.Count;
                ViewBag.PossibleCombinations = possibleCombinations;

                ViewBag.SearchMatch = secretPhrase; 
            }
            catch (Exception e)
            {
                // Save error to be displayed to user
                ViewBag.ErrorMessage = "Error Occurred: " + e.Message;
                ViewBag.SearchMatch = "Unkown Result";

                Debug.Write("Home Controller: ERROR - " + e.Message + "\n");

            }            

            // Record how long it took to process so we can show it to the user
            sw.Stop();
            ViewBag.Elapsed = sw.Elapsed;

            Debug.Write("Home Controller: STOP STOPWATCH - ELASPED TIME: " + sw.Elapsed + "\n");

            return View();
        }
    }
}