﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TheRedPill.Startup))]
namespace TheRedPill
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
