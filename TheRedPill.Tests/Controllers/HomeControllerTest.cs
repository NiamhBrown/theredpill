﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheRedPill;
using TheRedPill.Controllers;
using TheRedPill.Library;

namespace TheRedPill.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        // Default Unit Test
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        // My Unit Tests
        [TestMethod]
        public void IsWholeWordInAnagram()
        {
            bool result = false;

            // Arrange
            List<string> words = new List<string>();
            WordCombiner combine = new WordCombiner(words, ConfigurationManager.AppSettings["SecretPhraseAnagram"]);

            // Possible word test
            result = combine.IsWholeWordInAnagram("try");
            Assert.IsTrue(result);

            // Impossible word test
            result = combine.IsWholeWordInAnagram("wizard");
            Assert.IsFalse(result);

            // Duplicate letter in word test
            result = combine.IsWholeWordInAnagram("anna");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsPossiblePhrase()
        {
            bool result = false;

            // Arrange
            List<string> words = new List<string>();
            WordCombiner combine = new WordCombiner(words, ConfigurationManager.AppSettings["SecretPhraseAnagram"]);

            // Possible phrase test
            words.Clear();
            words.Add("an");
            words.Add("try");
            words.Add("out");
            words.Add("wits");
            words.Add("poul");
            result = combine.IsPossiblePhrase(words);
            Assert.IsTrue(result);

            // Impossible phrase test
            words.Clear();
            words.Add("van");
            words.Add("xtry");
            words.Add("wout");
            words.Add("ywits");
            words.Add("zpoul");
            result = combine.IsPossiblePhrase(words);
            Assert.IsFalse(result);
        }
    }
}
